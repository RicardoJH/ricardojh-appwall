'use strict';

Template.form.events({
    'submit .add-new-post': function (event) {
        //listens for a SUBMIT event and references class of add-new-post
        event.preventDefault();
        //stops page from automatically refreshing

        var postImage = event.currentTarget.children[0].files[0];
        var postName = event.currentTarget.children[1].value;
        var postMessage = event.currentTarget.children[2].value;

        Collections.Images.insert(postImage, function (error, fileObject) {
            if (error) {
                alert("Process Failure");
            } else {
                Collections.Name.insert({
                    name: postName,
                    message: postMessage,
                    createdAt: new Date(),
                    imageID: fileObject._id
                });
                $('.grid').masonry('reloadItems');
                //makes sure new image is displayed properly
            }
        });
    }
});