"use strict";

Template.posts.helpers({
    postList: function () {
        return Collections.Name.find({});
    },

    updateMasonry: function () {
        $('.grid').imagesLoaded().done(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                gutter: 15,
                columnWidth: '.grid-sizer',
                percentPosition: true
            });
        });
    },

    postImage: function () {
        var migimage = Collections.Images.find({
            _id: this.imageID
        }).fetch();
        return migimage[0].url();
    }
});